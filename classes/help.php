<?php
include("bootstrapfunc.php");
bootstraphead();
bootstrapbegin("Hilfe");
echo "<a href='../index.php' class='btn btn-primary btn-sm active' role='button'>Menü</a> "; 
echo "<a href='showtab.php?menu=database' class='btn btn-primary btn-sm active' role='button'>Datenbanken</a><br><br> "; 
echo "<br>";
echo "Die 1. Datenbank enthält die alte Struktur<br>";
echo "Die 2. Datenbank enthält die neue Struktur<br>";
echo "<br>In diesem Fall wäre es wie folgt:<br>";
$arrdatabase=array();
$database="../data/finddbchanges.db";
$db = new SQLite3($database);
$query="SELECT * FROM tbldatabase WHERE fldaktiv='J' ORDER BY fldsort";
$results = $db->query($query);
while ($row = $results->fetchArray()) {
  $arrdatabase[]=$row['fldpfad'].$row['fldbez'];
}
$db->close();
echo "Die Datenbank '".$arrdatabase[0]."' enthält die alte Struktur<br>";
echo "Die Datenbank '".$arrdatabase[1]."' enthält die neue Struktur<br>";

bootstrapend();
?>