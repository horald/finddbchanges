<?php

function tablelist() {
  echo "<br>";
  date_default_timezone_set("Europe/Berlin");
  $timestamp = time();
  $datum = date("d.m.Y",$timestamp);
  $uhrzeit = date("H:i",$timestamp);
  struktur_erstellen("w","-- dbupdate-Script vom ".$datum." um ".$uhrzeit);
  $arrdatabase=array();
  $database="../data/finddbchanges.db";
  $db = new SQLite3($database);
  $query="SELECT * FROM tbldatabase WHERE fldaktiv='J' ORDER BY fldsort";
  $results = $db->query($query);
  while ($row = $results->fetchArray()) {
	 $arrdatabase[]=$row['fldpfad'].$row['fldbez'];
  }
  $db->close();

  $anzds=sizeof($arrdatabase);
  for($i = 0; $i < $anzds;$i++) {
	if ($i==0) {
      echo "<div class='alert alert-info'>";
      echo $arrdatabase[$i]." (alte Struktur)<br>";
	  echo "</div>";
	  $db = new SQLite3($arrdatabase[$i]);
      $query="SELECT name FROM sqlite_master WHERE type='table'";
      $results = $db->query($query);
      while ($row = $results->fetchArray()) {
	     $arrtable[]=$row['name'];
      }
      $db->close();
	}
	if ($i==1) {
      echo "<div class='alert alert-info'>";
      echo $arrdatabase[$i]." (neue Struktur)<br>";
	  echo "</div>";
      struktur_erstellen("a","-- for database ".$arrdatabase[$i]);
      $db = new SQLite3($arrdatabase[$i]);
      $query="SELECT name FROM sqlite_master WHERE type='table'";
      $results = $db->query($query);
      echo "<div class='alert alert-info'>";
	   echo "<br>Tabellen prüfen...<br>";
      while ($row = $results->fetchArray()) {
        if (!in_array($row['name'], $arrtable)) {
          echo "<br>Table '".$row['name']."' nicht gefunden.<br>";
		    $col=struktur_ermitteln("1",$db,$arrdatabase[0],$row['name'],"CREATE");
          struktur_erstellen("a",$col);
	     } else {
		    if (struktur_pruefen($arrdatabase[0],$db,$row['name'])) {
		      echo "<br>Table '".$row['name']."' hat eine geänderte Struktur.<br>";
		      $col=struktur_ermitteln("1",$db,$arrdatabase[0],$row['name'],"CREATENEW");
            struktur_erstellen("a",$col);
			   $col="INSERT INTO ".$row['name']."_new SELECT * FROM ".$row['name'].";";
			   echo $col."<br>";
            struktur_erstellen("a",$col);
			   $col="DROP TABLE ".$row['name'].";";
			   echo $col."<br>";
            struktur_erstellen("a",$col);
            $col="ALTER TABLE ".$row['name']."_new RENAME TO ".$row['name'].";";
			   echo $col."<br>";
            struktur_erstellen("a",$col);
		    }
		  }
      }
      echo "</div>";
      $db->close();
	}
	if ($i>1) {
      echo "<div class='alert alert-warning'>";
      echo "Datenbank '".$arrdatabase[$i]."' wird nicht berücksichtig!<br>";
	  echo "</div>";
	}
  } 
  if ($anzds==0) {
    echo "<div class='alert alert-warning'>";
    echo "Keine Datenbanken hinterlegt!<br>";
    echo "</div>";
  } 
  
}

function struktur_pruefen($database1,$db,$table) {
  $db1 = new SQLite3($database1);
  $lok=false;
  $query="pragma table_info('".$table."');";
  $res1 = $db1->query($query);
  $arrstruc=array();
  while ($row = $res1->fetchArray()) {
    $col="ALTER TABLE '".$table."' FIELD ".$row['cid']." '".$row['name']."' ".strtoupper($row['type']);
	if ($row['notnull']==1) {
	  $col=$col." NOT NULL";
	} else {
	  $col=$col." NULL";
	}
	if ($row['dflt_value']<>"") {
	  $col=$col." DEFAULT '".$row['dflt_value']."'";
	}
	if ($row['pk']==1) {
	  $col=$col." PRIMARY KEY AUTOINCREMENT";
	}
	$arrstruc[]=$col;
  }
  $db1->close();
  $results = $db->query($query);
  while ($row = $results->fetchArray()) {
    $col="ALTER TABLE '".$table."' FIELD ".$row['cid']." '".$row['name']."' ".strtoupper($row['type']);
	if ($row['notnull']==1) {
	  $col=$col." NOT NULL";
	} else {
	  $col=$col." NULL";
	}
	if ($row['dflt_value']<>"") {
	  $col=$col." DEFAULT '".$row['dflt_value']."'";
	}
	if ($row['pk']==1) {
	  $col=$col." PRIMARY KEY AUTOINCREMENT";
	}
	if (!in_array($col, $arrstruc)) {
	  $lok=true;
    }
  }
  return $lok;
}

function struktur_ermitteln($dbnr,$db,$database,$table,$typ) {
  if ($dbnr=="0") {
    $db = new SQLite3($database);
  }
  $query="pragma table_info('".$table."');";
  $results = $db->query($query);
  $col="";
  if ($typ=="CREATE") {
    $col="CREATE TABLE '".$table."' (";
  }
  if ($typ=="CREATENEW") {
    $col="CREATE TABLE '".$table."_new' (";
  }
  $cnt=0;
  while ($row = $results->fetchArray()) {
    $cnt=$cnt+1;
	if ($cnt==1) {
	  $col=$col."'".$row['name']."' ".$row['type'];
	} else {
	  $col=$col.",'".$row['name']."' ".$row['type'];
	}
	if ($row['notnull']==1) {
	  $col=$col." NOT NULL";
	} else {
	  $col=$col." NULL";
	}
	if ($row['dflt_value']<>"") {
	  $col=$col." DEFAULT ".$row['dflt_value'];
	}
	if ($row['pk']==1) {
	  $col=$col." PRIMARY KEY AUTOINCREMENT";
	}
  }	  
  $col=$col.");";
  echo $col."<br>";
  if ($dbnr=="0") {
    $db->close();
  }	
  return $col;
}

function struktur_erstellen($opentyp,$text) {
  $datei = fopen("../sites/export/dbupdate.sql",$opentyp);
  fwrite($datei, $text."\r\n");
  fclose($datei);
}

?>