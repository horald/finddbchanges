<?php
  include("bootstrapfunc.php");
  bootstraphead();
  bootstrapbegin("Installation");
  $dir=getcwd();
  $dir=substr($dir, 0, -8);

  $lok=true;
  $configfile = $dir."/config.php";
  $configfile_sample = $dir."/config_sample.php"; 
  if (filesize($configfile) == 0 ) {
    if (!copy($configfile_sample, $configfile)) {
      echo "<div class='alert alert-danger'>";
      echo "Kopieren der Konfigurationsdatei '".$configfile_sample."' ist fehlgeschlagen!<br>";
      echo "Bitte manuell nach '".$configfile."' umkopieren.";
      echo "</div>";
      $lok=false;
    }
  }	
  if ($lok) {
    $file = $dir.'/sites/install/finddbchanges.db';
    $newfile = $dir.'/data/finddbchanges.db';
    mkdir($dir."/data/");

    if (!copy($file, $newfile)) {
      echo "<div class='alert alert-warning'>";
      echo "copy $file schlug fehl...\n<br>";
      echo "Installation der Datenbank (".$newfile.") fehlgeschlagen!";
      echo "</div>";
    } else {
      echo "<div class='alert alert-success'>";
      echo "Datenbank erfolgreich installiert. (".$newfile.")";
      echo "</div>";
    }
  }  
  echo "<a href='../index.php' class='btn btn-default'>Neustart</a>";
  bootstrapend();  
?>