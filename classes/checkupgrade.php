<?php

function checkinstall() {
  $lok=true;	
  $dbFile="config.php";
  if (filesize($dbFile) == 0 ) {
  	 $lok=false;
  }
  $dir=getcwd();
  $newfile = $dir.'/data/finddbchanges.db';
  if (filesize($newfile) == 0 ) {
  	 $lok=false;
  } 
  if (!$lok) {	
    echo "<div class='alert alert-warning'>";
    echo "Programm noch nicht konfiguriert! Jetzt installieren?";
    echo "</div>";
    echo "<form class='form-horizontal' method='post' action='classes/install.php'>";
    echo "<dd><input type='submit' value='Installieren' /></dd>";
    echo "</form>";
  }   
  return $lok;
}

?>